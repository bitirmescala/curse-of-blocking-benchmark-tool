package controllers

import play.api._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import play.api.libs.ws.{DefaultWSClientConfig, WS}
import play.api.mvc._
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object Application extends Controller {

  def index(reqs:Int, maxConnections: Option[Int]) = Action.async {
    val allStart = System.currentTimeMillis()

    val clientConfig = new DefaultWSClientConfig()
    val secureDefaults:com.ning.http.client.AsyncHttpClientConfig = new NingAsyncHttpClientConfigBuilder(clientConfig).build()
    // You can directly use the builder for specific options once you have secure TLS defaults...
    val builder = new com.ning.http.client.AsyncHttpClientConfig.Builder(secureDefaults)

    maxConnections foreach builder.setMaximumConnectionsPerHost;

    val secureDefaultsWithSpecificOptions:com.ning.http.client.AsyncHttpClientConfig = builder.build()
    implicit val implicitClient = new play.api.libs.ws.ning.NingWSClient(secureDefaultsWithSpecificOptions)

    val futures =  for(i <- 1 to reqs) yield {
      val start = System.currentTimeMillis()
      WS.clientUrl("http://localhost:9000/").get().map(_ => (start, System.currentTimeMillis())).recover{ case _ => (0l, 0l) }
    }
    val future = Future.sequence(futures)
    future.map { vals =>
      val durations = vals.map{ case (start,end) => end-start}
      val (failed,successful) = durations.toList.partition( _ == 0)

      val average = successful.sum/successful.size
      val max = durations.max
      val totalTime = System.currentTimeMillis() - allStart
      val response =
        s"""Request count: $reqs
           |Failed Reqs: ${failed.size}
           |Successful : ${successful.size}
           |Total time: $totalTime
           |Average Resp: $average
           |Max Response: $max
           |
           |All durations:
           |${durations.mkString("\n")}
         """.stripMargin
      implicitClient.close()
      Ok(response)
    }

  }

}
